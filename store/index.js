import axios from "axios"

export const state = () => ({
  restaurants: [],
  filterRestaurants: [],
  hits: [],
  reviews: []
})

export const actions = {
  request({ commit }, payload) {
    axios
      .get(payload.request)
      .then(response => {
        commit(payload.commit, response.data)
      })
      .catch(error => {
        window.alert(error)
      })
  }
}

export const mutations = {
  setRestaurants(state, payload) {
    state.restaurants = state.filterRestaurants = payload
  },
  setHits(state, payload) {
    state.hits = payload
  },
  setReviews(state, payload) {
    state.reviews = payload
  },
  filter(state, payload) {
    const search = (item, payload) =>
      item.Name.toLowerCase().includes(payload.search.toLowerCase())
    const filter = (item, payload) => {
      const result = payload.filter.filter(v =>
        item.Specializations.some(v2 => v === v2.Name)
      )
      return payload.filter.length !== 0 ? result.length !== 0 : true
    }
    state.filterRestaurants = state.restaurants
      .slice()
      .filter(item => search(item, payload) && filter(item, payload))
  }
}

export const getters = {
  getRestaurants: state => state.filterRestaurants,
  getHits: state => state.hits,
  getReviews: state => state.reviews
}
