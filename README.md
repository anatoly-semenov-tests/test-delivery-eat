# delivery-eat
> Сборка
```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

> Задача

Cделать SPA, имитирующее существющую страницу всех ресторанов CHIBBIS.RU (https://angarsk.chibbis.ru/restaurants)
![alt text](assets/img/readme.png)
### API:
> Рестораны:   GET https://front-task.chibbistest.ru/api/v1/restaurants
>
> Отзывы:      GET https://front-task.chibbistest.ru/api/v1/reviews
>
> Хиты продаж: GET https://front-task.chibbistest.ru/api/v1/hits


### Функции:
>Фильтры: Бесплатная доставка, Пицца, Суши, Бургеры, Пироги
>
>Поиск: по названию

### Требования

> JS, React / Vue
>
> SSR
>
> (Нужно делать fetch трёх гетов на серверной стороне и отдавать на клиент готовую страницу с этими блоками)
>
> CSS препроцессор на выбор (sass / less / stylus)
>
> Именование классов по БЭМ (или любой другой альтернативной методологии)
>
> Responsive (min width = 320px)
>
> Опционально - Роутинг
