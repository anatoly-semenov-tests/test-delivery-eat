import Vue from "vue"
import {
  Layout,
  Breadcrumb,
  Checkbox,
  Input,
  Card,
  Avatar,
  Icon,
  List,
  Tooltip,
  Comment,
  Skeleton
} from "ant-design-vue/lib"

Vue.use(Layout)
Vue.use(Breadcrumb)
Vue.use(Checkbox)
Vue.use(Input)
Vue.use(Card)
Vue.use(Avatar)
Vue.use(Icon)
Vue.use(List)
Vue.use(Tooltip)
Vue.use(Comment)
Vue.use(Skeleton)
